import './LoginForm.css';
import emailIcon from '../images/emailIcon.png';
import facebookIcon from '../images/facebookIcon.png';

const SignInForm = (clicked) => {

    return (
        <div className='loginFormStyleActive' >
            <h1 style={{ fontWeight: 'normal', marginBottom: '5px' }}>Hello there,</h1>
            <h1 style={{ fontWeight: 'normal', marginTop: '0px', marginBottom: '35px' }}>Welcome to <label style={{ color: 'white', fontWeight: 'bold', fontSize: '40px' }}>MySoftware!</label></h1>
            <div className='formInputs'>
                <div className='formInputStyle'>
                    <label>User</label>
                    <input type='text' name='user' id='user'></input>
                </div>
                <div className='formInputStyle'>
                    <label>Password</label>
                    <input type='password' name='password' id='password'></input>
                </div>
            </div>
            <div className='linksInput'>
                <label>
                    <input type="checkbox"></input>
                    Remember Me
                </label>
                <div className='forgotLink'>
                    <a href='#'>Forgot Password?</a>
                </div>
            </div>
            <button type='submit'>Login</button>
            <div className='socialMedia'>
                <p className='socialMediaText'>Or connect with Social Media</p>
                <div>
                    <img src={emailIcon} alt='emailIcon' width={35} height={35} color='white'></img>
                    <img src={facebookIcon} alt='facebookIcon' width={35} height={35}></img>
                </div>
            </div>
        </div >
    )


}

export default SignInForm;