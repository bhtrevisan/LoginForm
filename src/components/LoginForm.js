import React, { useState } from 'react';
import './LoginForm.css';
import SignInForm from './SignInForm';
import SignUpForm from './SignUpForm';
import SignUpCall from './Callouts/SignUpCall';
import LoginCall from './Callouts/LoginCall';



const LoginForm = () => {
    const [clicked, setClicked] = useState(false);
    const handleClick = () => setClicked(!clicked);
    return (
        <div className='loginForm'>
            <div className='backgroundBox'>
                <div className={clicked ? 'box1' : 'box1Active'} >
                    {clicked ? <SignUpForm clicked={clicked} /> : <SignInForm clicked={clicked} />}
                </div>

                <div className={clicked ? 'box2Active' : 'box2'} >
                    {clicked ? <LoginCall handleClick={handleClick} /> : <SignUpCall handleClick={handleClick}/>}
                    {/* <button onClick={handleClick}>{clicked ? 'Login' : 'Sign Up'}</button> */}
                </div>
            </div>
        </div >
    )
}



export default LoginForm;