import React from "react";
import LoginForm from "./components/LoginForm";
import "./App.css";
import NavBar from "./components/NavBar";

function App() {

  return (
    <div>
      <NavBar />
      <LoginForm />
    </div>
  );
}

export default App;
