import './Callouts.css';

const SignUpCall = (props) => {

    return (
        <div className="callout">
            <h1>Don't have an account?</h1>
            <p>Come and join us in this beatiful journey</p>
            <button onClick={props.handleClick}>Sign Up</button>
        </div>
    )
}

export default SignUpCall;