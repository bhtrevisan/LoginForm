## Login Form

## 📜 Description
I'm starting this to practice some animations with CSS and to have a cool login page for future projects.
This is just being used for the layout, it is still is not working as a login page.

## 🖼️ Screenshots
I'll add some prints to show how this project looks.

### Login Screen
![LoginScreen](./src/images/LoginScreen.png)

### Signup Screen
![SignupScreen](./src/images/SignUpScreen.png)

## 💻 Requirements
You will need:
* npm;
* react;

## 🔨 Installation 
To install this project in your local machine, follow this steps:

Create a folder in your local machine.
Then run

`git clone {this project link}`

Go to project folder.

`npm install`


## ☕ Using Project
To use this project, you need to start this application on your localhost.

Go to project folder, which has te package.json and run:

`npm start`

## 🔧 Fixes and new features
This project is under development and the next steps are:
- Add a backend to allow creation of account;
- Add a backend to validate the login;
- Add a database to store users;

## 🙂 Author
[@bhtrevisan](https://gitlab.com/bhtrevisan)