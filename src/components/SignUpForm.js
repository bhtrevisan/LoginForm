import './LoginForm.css';
import emailIcon from '../images/emailIcon.png';
import facebookIcon from '../images/facebookIcon.png';

const SignUpForm = (clicked) => {

    return (
        <div className='signUpFormStyleActive'>
            <h1>Create Account</h1>
            <p style={{ fontWeight: 'regular', opacity: '0.7' }}>It takes less than a minute to join us.</p>
            <div className='formInputs'>
                <div className='formInputStyle'>
                    <label>Name</label>
                    <input type='text' name='name' id='name'></input>
                </div>
                <div className='formInputStyle'>
                    <label>Email</label>
                    <input type='email' name='email' id='email'></input>
                </div>
                <div className='formInputStyle'>
                    <label>Password</label>
                    <input type='password' name='password' id='password'></input>
                </div>
                <label style={{ fontSize: '12px', fontWeight: 'normal' }}>
                    <input type="checkbox"></input>
                    I agree with the terms of use.
                </label>
            </div>
            <button type='submit'>Sign Up</button>
            <div className='socialMedia'>
                <p className='socialMediaText'>Or connect with Social Media</p>
                <div>
                    <img src={emailIcon} alt='emailIcon' width={35} height={35} color='white'></img>
                    <img src={facebookIcon} alt='facebookIcon' width={35} height={35}></img>
                </div>
            </div>
        </div>
    )


}

export default SignUpForm;