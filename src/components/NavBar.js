import './NavBar.css';
import BHTLogo from '../images/BHT_Logo.png';

const NavBar = () => {

    return (
        <div className='navBarBackground'>
            {/* <div className='empty'></div> */}
            <div className='navLogo'>
                <img src={BHTLogo} className='logo'></img>
            </div>
            <div className='navLinks'>
                <label>About</label>
                <label>Our Services</label>
                <label>Our Work</label>
                <label>Contact</label>
            </div>
        </div>
    )

}

export default NavBar;