import './Callouts.css';

const LoginCall = (props) => {

    return (
        <div className="callout">
            <h1>Already have an account?</h1>
            <p>Login with your personal information.</p>
            <button onClick={props.handleClick}>Login</button>
        </div>
    )

}

export default LoginCall;